# Front-End Website Development Training Resources

## Author(s): Henry Ge

## Overview
The purpose of this repository is to provide training resources for HTML, CSS, and Javascript as well as principles of aesthetic design for use in the FIRST Robotics Competition.

HTML, CSS, and Javascript are a powerful coding triumvirate that allow for the creation of beautiful and responsive websites. HTML gives the website structure, CSS gives the website aesthetics, and Javascript gives the website functionality.

HTML Version 5 and CSS Version 3 will be used for these training resources due to their widespread use at the making of this repository. JQuery will be used instead of Vanilla Javascript because of its ease-of-use and vast documentation on the internet.

These training resources will also discuss principles of web design and introduce the basic concepts of GUI development.

The ultimate coal of these Training Resources is to teach the fundamentals of front-end (client-side) development.

In addition, the training resources will integrate mobile website design, including the AMP project.

## Resources Required
- A Windows or Linux Machine
- Internet Access
- Notebook & Pencil

## Setup Procedure
- Go to Cloud9, Codenvy, or any other online IDE
    - IMPORTANT: If using Cloud9, be sure to go to https://c9.io/login version, not the AWS Cloud9 Web Services version.
- If using Cloud9, Login to the team account with the credentials specified by your Lead
    - If using Codenvy or any other online IDE, please create a personal account based on instructions from the respective website.
    - NOTE: It is advised that you use Cloud9 to be consistent with the training resources.
- Create a new workspace
    - If using the Cloud9 Team Account, please name the workspace with your name followed by the current FIRST Robtics season year followed by "Training". Example: JohnDoe-2019-Training.
    - Choose the HTML5 Template.
- Create a new file with a file extension ".html". This will be the HTML document to work on.
    - Additionally create a folder titled "graphics" and create a file with extension ".css" inside. This will be the CSS document to work on.
    - Additionally create a folder titled "js" and create a file with extension ".js" inside. This will be the Javascript document to work on.
    
## Basic Usage
- Create new files by typing in their full name with the file extension.
    - If using anything other than Cloud9, refer to the respective Online IDE's instructions
- To preview the code, right click on the HTML document and select "preview" to see how the code translates into a GUI on screen.
    - IMPORTANT: The preview opiton will not work with any back-end code such as Django, PHP, Node.js, or Ruby. If using any back-end code, click "Run" at the top. Look to the console and there should be a clickable link.

## Curriculum Materials
- If you are a lead or team member in search of detailed tutorials or the curriculum (in powerpoints, etc.), please refer to the Google Folder materials in conjunction with the resources here on GitLab.
    - Ask your lead for the Google Folder if you have trouble accessing the curriculum.

## General Documentation
- HTML 5: https://developer.mozilla.org/en-US/docs/Web/HTML
- CSS 3: https://www.w3schools.com/cssref/default.asp
- Javascript: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference
- JQuery: https://api.jquery.com/
- AMP Project: https://www.ampproject.org/docs/